CanaRx was the first International Prescription Service Provider (IPSP) to introduce and supply a public sector program in America. Founded in 2002, our goal at CanaRx was to provide Americans with access to safe and affordable brand name medications from Tier One countries.

Website : http://www.canarx.com